.. _user-software-origins-nixguix:

Nix and Guix
============

.. todo::
   This page is a work in progress.

.. include:: dynamic/nixguix_status.inc

TODO:

* description of the software origin
* summary of the lister's algorithm
* summary of the loader's algorithm
* URL pattern
* collect extrinsic metadata?
* index extrinsic metadata?
* index intrinsic metadata?
